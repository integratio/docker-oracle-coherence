FROM integratio/centos-java7

RUN yum -y install unzip

# add coherence zip to container
ADD ofm_coherence_quick_generic_12.1.2.0.0_disk1_1of1.zip /opt/oracle/

# unzip coherence installation file
RUN unzip /opt/oracle/ofm_coherence_quick_generic_12.1.2.0.0_disk1_1of1.zip -d /tmp/coherence-install/

# create user to install and run coherence
RUN groupadd coherence
RUN useradd -g coherence coherence

# prepare oracle home for coherence
RUN mkdir -p /opt/oracle/coherence
RUN chown coherence /opt/oracle/coherence/
RUN chgrp coherence /opt/oracle/coherence/

# apply socket buffer size tuning
#RUN sysctl -w net.core.rmem_max=2097152
#RUN sysctl -w net.core.rmem_max=2097152
#
# run the oracle universal installer as the coherence user
# (refuses to run as root)
RUN runuser -l coherence -c 'java -jar /tmp/coherence-install/coherence_quick_121200.jar ORACLE_HOME=/opt/oracle/coherence'

RUN sed -i -e 's/JAVAEXEC -server/JAVAEXEC -server -Dtangosol.coherence.clusteraddress=237.0.0.1 -Dtangosol.coherence.clusterport=9000/g' /opt/oracle/coherence/coherence/bin/cache-server.sh

EXPOSE 8088

CMD /opt/oracle/coherence/coherence/bin/cache-server.sh
